package com.example.ChatMicroservice.messages.web.rest.v1;

import com.example.ChatMicroservice.messages.data.ChatMessage;
import com.example.ChatMicroservice.messages.data.MessageCongregate;
import com.example.ChatMicroservice.messages.data.entities.Message;
import com.example.ChatMicroservice.messages.data.service.AuthorizationService;
import com.example.ChatMicroservice.messages.data.service.MessageService;
import com.example.ChatMicroservice.messages.data.service.WebSocketService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MessageController {
    private final WebSocketService webSocketService;
    private final MessageService messageService;
    private final AuthorizationService authService;

    @MessageMapping("/message") // mapped as app/message
    public void sendToSpecificUser(@RequestBody ChatMessage message) {
        this.webSocketService.sendMessage(message);
    }

    @MessageMapping("/typing")
    public void sendTypingNotification(@RequestBody ChatMessage message) {
        this.webSocketService.sendTypingNotification(message);
    }

    @MessageMapping("/seen")
    public void markAsSeen(@RequestBody MessageCongregate messageCongregate) {
        this.messageService.markAsSeen(messageCongregate.getLastSeenMessageId());
        this.webSocketService.sendSeenNotification(messageCongregate.getUserId(),
                messageCongregate.getLastSeenMessageId());
    }


    // mocks cu oauth2
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping("/{userId}/messageHistory")
    public ResponseEntity<List<Message>> getUserHistory(@PathVariable Integer userId,
                                                        @RequestHeader("Authorization") String jwtToken) {
        if (this.authService.isAuthenticatedUser(jwtToken.substring(7))) {
            return ResponseEntity.ok(messageService.getUserHistory(userId));
        } else return ResponseEntity.status(403).body(null);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/save")
    public ResponseEntity<Integer> saveMessage(@RequestBody ChatMessage message, @RequestHeader("Authorization") String jwtToken) {
        if (this.authService.isAuthenticatedUser(jwtToken.substring(7))) {
            return ResponseEntity.ok(this.messageService.saveMessage(
                    Message.builder()
                            .senderId(message.getSenderId())
                            .receiverId(message.getReceiverId())
                            .text(message.getText())
                            .seen(false)
                            .build()
            ));
        } else return ResponseEntity.status(403).body(null);
    }
}

