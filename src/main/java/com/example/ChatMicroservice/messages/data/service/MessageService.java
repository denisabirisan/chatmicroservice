package com.example.ChatMicroservice.messages.data.service;

import com.example.ChatMicroservice.messages.data.entities.Message;
import com.example.ChatMicroservice.messages.data.repos.MessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;

    public Integer saveMessage(Message message) {
        this.messageRepository.save(message);
        return message.getId();
    }

    public List<Message> getUserHistory(Integer userId) {
        return this.messageRepository.findAll()
                .stream()
                .filter(mesaj -> Objects.equals(mesaj.getReceiverId(), userId) || Objects.equals(mesaj.getSenderId(), userId))
                .toList();
    }

    public void markAsSeen(Integer lastSeenMessageId) {
        var lastSeen = this.messageRepository.findFirstById(lastSeenMessageId);
        lastSeen.setSeen(true);
        this.messageRepository.save(lastSeen);
    }
}
