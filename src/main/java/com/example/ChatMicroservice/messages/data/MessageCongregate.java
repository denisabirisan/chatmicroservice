package com.example.ChatMicroservice.messages.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageCongregate {

    private Integer userId;
    private Integer lastSeenMessageId;
}
