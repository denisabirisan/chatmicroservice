package com.example.ChatMicroservice.messages.data.repos;

import com.example.ChatMicroservice.messages.data.entities.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {
    List<Message> findAllBySenderId(Integer senderId);

    Message findFirstById(Integer id);
}
