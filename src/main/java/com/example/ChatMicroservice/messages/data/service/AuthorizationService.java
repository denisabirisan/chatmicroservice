package com.example.ChatMicroservice.messages.data.service;

import com.example.ChatMicroservice.messages.data.Constants;
import com.example.ChatMicroservice.messages.data.entities.JwtTokenEntity;
import com.example.ChatMicroservice.messages.data.repos.AuthorizationRepository;
import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
@RequiredArgsConstructor
public class AuthorizationService {
    private final AuthorizationRepository authRepo;
    private final RestTemplate restTemplate;

    public boolean isAuthenticatedUser(String jwtToken) {
        Claims claims = JwtDecoder.decodeJwt(jwtToken);

        String email = claims.getSubject();
        if (this.isSavedToken(jwtToken)) return true;
        else {
            var permission = this.askAuthMicroserviceForPermission(jwtToken);
            if (permission.equals(HttpStatusCode.valueOf(200))) {
                this.authRepo.save(JwtTokenEntity.builder().jwtToken(jwtToken).build());
                return true;
            } else return false;
        }
    }

    public boolean isSavedToken(String jwt) {
        return this.authRepo.findFirstByJwtToken(jwt) != null;
    }

    public HttpStatusCode askAuthMicroserviceForPermission(String jwtToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        headers.set("Authorization", "Bearer " + jwtToken);

        var userUrl = UriComponentsBuilder.fromUriString(Constants.BASE_URL).pathSegment("auth").build().toUriString();

        HttpEntity<Object> requestEntity = new HttpEntity<>(null, headers);

        ResponseEntity<Boolean> response = restTemplate.exchange(userUrl, HttpMethod.GET, requestEntity, Boolean.class);
        return response.getStatusCode();
    }
}
