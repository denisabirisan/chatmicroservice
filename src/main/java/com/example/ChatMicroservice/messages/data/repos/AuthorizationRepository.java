package com.example.ChatMicroservice.messages.data.repos;

import com.example.ChatMicroservice.messages.data.entities.JwtTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorizationRepository extends JpaRepository<JwtTokenEntity, Integer> {
    JwtTokenEntity findFirstByJwtToken(String jwtToken);
}
