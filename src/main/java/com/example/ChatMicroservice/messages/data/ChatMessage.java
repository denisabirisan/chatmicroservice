package com.example.ChatMicroservice.messages.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ChatMessage {
    private Integer id;
    private Integer senderId;
    private Integer receiverId;
    private String text;
    private Boolean seen;
}
