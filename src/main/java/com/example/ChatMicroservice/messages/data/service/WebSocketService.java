package com.example.ChatMicroservice.messages.data.service;

import com.example.ChatMicroservice.messages.data.ChatMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class WebSocketService {
    private final SimpMessagingTemplate messagingTemplate;
    private final MessageService messageService;

    public void sendMessage(ChatMessage mesaj) {
        messagingTemplate.convertAndSendToUser(mesaj.getReceiverId().toString(), "/queue/messages", mesaj);
    }

    public void sendTypingNotification(ChatMessage message) {
        messagingTemplate.convertAndSendToUser(message.getReceiverId().toString(), "/queue/typing", message);
    }

    public void sendSeenNotification(Integer userId, Integer lastSeenMessage) {
        messagingTemplate.convertAndSendToUser(userId.toString(), "/queue/seen", lastSeenMessage);
    }
}
