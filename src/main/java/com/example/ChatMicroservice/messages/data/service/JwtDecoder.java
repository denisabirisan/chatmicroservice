package com.example.ChatMicroservice.messages.data.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

public class JwtDecoder {
    private static final String SECRET_KEY = "0c6f3603a8614d21ce76f0012980289f9ea4d77fd61883cecd943142292bf279";

    public static Claims decodeJwt(String token) {
        return Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
    }
}